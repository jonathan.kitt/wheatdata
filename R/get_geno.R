#' Get genotypes
#'
#' @param chr chromosome name (e.g. "chr1A" or c("chr1A", "chr2A")).
#' @param chr_nb return all chromosomes with the same number (e.g. "1" will return chr1A, chr1B and chr1D).
#' @param genome return all chromosomes from a subgenome (e.g. "A" will return chr1A, chr2A, ..., chr7A).
#' @param codes return results for a selection of samples (e.g. "ERGE1019").
#'
#' @import data.table
#' @import dplyr
#' @import stringr
#' @import tibble
#' @import tidyr
#'
#' @return table of genotypes
#' @export
#'
#' @examples
#' \dontrun{
#" get_geno(chr = "chr1A", codes = "ERGE1019")
#' }

get_geno <- function(chr = NULL, chr_nb = NULL, genome = NULL, codes = NULL) {

  all_chr <- tibble::tibble(
    chrom = c("chr1A", "chr2A", "chr3A", "chr4A", "chr5A", "chr6A", "chr7A",
              "chr1B", "chr2B", "chr3B", "chr4B", "chr5B", "chr6B", "chr7B",
              "chr1D", "chr2D", "chr3D", "chr4D", "chr5D", "chr6D", "chr7D"))

  if (is.null(chr) & is.null(genome) & is.null(chr_nb)) {
    chr_list <- dplyr::pull(all_chr)
  }

  if (!is.null(chr) & length(chr) == 1 & is.null(genome) & is.null(chr_nb)) {
    chr_list <- all_chr |>
      dplyr::filter(chrom == chr) |>
      dplyr::pull()
  }

  if (!is.null(chr) & length(chr) > 1 & is.null(genome) & is.null(chr_nb)) {
    chr_list <- all_chr |>
      dplyr::filter(chrom %in% chr) |>
      dplyr::pull()
  }

  if (is.null(chr) & !is.null(genome) & is.null(chr_nb)) {
    chr_list <- unlist(stringr::str_extract_all(string = all_chr$chrom,
                                                pattern = paste0("chr[1-7]", genome)))
  }

  if (is.null(chr) & is.null(genome) & !is.null(chr_nb)) {
    chr_list <- unlist(stringr::str_extract_all(string = all_chr$chrom,
                                                pattern = paste0("chr", chr_nb, "[ABD]")))
  }

  if (is.null(codes)) {
    smp_list <- samples$sample_code
  }

  if (!is.null(codes)) {
    smp_list <- codes
  }

  d1_list <- list()

  for (i in chr_list) {
    # file_name <- paste0("genotypes_", i)
    d1_list[[i]] <- get0(paste0("genotypes_", i), envir = asNamespace("wheatdata")) |>
    # d1_list[[i]] <- load(paste0("wheatdata::genotypes_", i)) |>
    # d1_list[[i]] <- get(load(paste0("data/genotypes_", i, ".rda"))) |>
      dplyr::select(probeset_id:position, dplyr::all_of(smp_list))
  }

  d1 <- tibble::as_tibble(data.table::rbindlist(d1_list))

  return(d1)

}
